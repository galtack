#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK runtime-loadable module.
"""
# ( TODO: Move the next three paragraphs into the docs or an independent
#   file.  Also, make sure to give appropriate credit to Phil Hassey. )

# This code contains bits from the Ludum Dare version of Galcon, which is
# licensed under the GNU GPL.  They are highlighted with "ld488" markers.
# Phil Hassey gave his permission to use the following bits under the terms
# of the MIT license:

# ld488: cnst.py:1
# ld488: cnst.py:11
# ld488: flock.pyp:55-103
# ld488: flock.pyp:128
# ld488: flock.pyp:130
# ld488: level.py:27
# ld488: level.py:28
# ld488: level.py:89-99

# Including more of ld488 needs prior permission by Phil Hassey.

# Copyright (C) 2007  Michael Carter
# Copyright (C) 2007  Felix Rabe <public@felixrabe.textdriven.com>
# Copyright (C) 2007  Phil Hassey  (originator of ld488 code)

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

import math

from galtack_client import *


class GaltackUniverseView(DrawingArea):
    """
    GalTacK universe canvas.
    """
    
    def __init__(self):
        super(GaltackUniverseView, self).__init__()
        self.reset()
        self.connect("expose-event", self.__cb_expose_event)
        self.set_size_request(640, 480)
    
    def reset(self):
        self.planet_info_list = []
    
    def __cb_expose_event(self, widget, event):
        x, y, w, h = self.allocation
        cr = self.window.cairo_create()
        
        # background
        cr.rectangle(0, 0, w, h)
        cr.set_source_rgba(0.1, 0.0, 0.2, 1)
        cr.fill()
        
        e = cr.font_extents()
        fascent, fdescent, fheight, fxadvance, fyadvance = e
        
        # planets
        for planet_info in self.planet_info_list:
            cr.set_source_rgba(1, 1, 1, 1)
            if planet_info.dispx != -1:
                px, py = planet_info.dispx, planet_info.dispy
            else:
                px, py = planet_info.x, planet_info.y
            cr.arc(px, py, planet_info.radius, 0, PI2)
            cr.fill()
            
            txt = "%u" % planet_info.num
            e = cr.text_extents(txt)
            xbearing, ybearing, width, height, xadvance, yadvance = e
            cr.move_to(px + 0.5 - xbearing - width / 2,
                       py + 0.5 - fdescent + fheight / 2)
            cr.set_source_rgba(1, 0, 0, 1)
            cr.show_text(txt)
    
    def redraw(self):
        if self.window is not None:
            self.window.invalidate_rect(self.allocation, False)
        return False


class GaltackUniverseWindow(WindowF5RawConsoleMixin):
    """
    A Window displaying a view of the GalTacK universe.
    """
    
    def __init__(self):
        super(GaltackUniverseWindow, self).__init__()
        self.set_title("Universe Window - GalTacK")
        self.universe_view = self(GaltackUniverseView())


class LoadableController(object):
    """
    Provide some shortcuts for interactive use.
    """
    
    def __init__(self, galtack_client, *a, **kw):
        self.gc = galtack_client
        self.uw = GaltackUniverseWindow()
        self.uv = self.uw.universe_view
    
    def __call__(self, *a):
        self.gc.send_commands(a)
    
    @staticmethod
    def __stabilize_planet_pos(planet_pos_list):
        SW, SH = 640,480    # ld488: cnst.py:1
        FPS = 32            # ld488: cnst.py:11
        # BORDER = 40         # ld488: level.py:27
        BORDER = 40
        PADDING = 8         # ld488: level.py:28
        # ld488: level.py:89-99
        for n in xrange(0,FPS):
            top = BORDER
            left = PADDING
            right = SW-left*2
            bottom = SH-top*2
            for i, (p, x, y) in enumerate(planet_pos_list):
                rad = p.radius
                x = min(max(x, left + rad), right - rad)
                y = min(max(y, top + rad), bottom - rad)
                planet_pos_list[i] = [p, x, y]
            LoadableController.__planet_flock_loop(planet_pos_list)
    
    @staticmethod
    def __planet_flock_loop(planet_pos_list):  # ld488: flock.pyp:55-103
        ## First 'for' loop unnecessary, as "p->loop = 0" in:
        ## ld488: flock.pyp:128
        enumerated = tuple(enumerate(planet_pos_list))
        for n, (ap, ax, ay) in enumerated:
            for m, (bp, bx, by) in enumerated[n+1:]:
                dx = bx - ax
                dy = by - ay
                r = ap.radius + bp.radius
                if abs(dx) > r or abs(dy) > r:
                    continue
                dist = math.sqrt(dx*dx + dy*dy)
                if dist < r:
                    ## no targets (but what about the part with index 0?)
                    if dist == 0:
                        dist = 1
                        dx = 1
                    ## w = weight and a->w == b->w == 100000 for
                    ## planets: ld488: flock.pyp:130.
                    ## Here, we cancel down to a->w == b->w == 1 (w = 2.0)
                    mx = (ax + bx) / 2.0
                    my = (ay + by) / 2.0
                    d2 = dist * 2.0
                    planet_pos_list[n][1] = mx - ((r * dx) / d2)
                    planet_pos_list[n][2] = my - ((r * dy) / d2)
                    planet_pos_list[m][1] = mx + ((r * dx) / d2)
                    planet_pos_list[m][2] = my + ((r * dy) / d2)
    
    def update(self):
        if self.gc.u64data is None:
            planets = []
        else:
            planets = self.gc.u64data.planets
        for p in planets:
            p.dispx, p.dispy = -1, -1
        self.uv.planet_info_list = planets
        self.uv.redraw()
        gobject.timeout_add(500, self.__update_2)
    
    def __update_2(self):
        if self.gc.u64data is None:
            planets = []
        else:
            planets = self.gc.u64data.planets
        planet_pos_list = []
        for p in planets:
            planet_pos_list.append([p, p.x, p.y])
        self.__stabilize_planet_pos(planet_pos_list)
        for i, p in enumerate(planets):
            p.dispx, p.dispy = planet_pos_list[i][1:]
        self.uv.planet_info_list = planets
        self.uv.redraw()


def run_loadable(*a, **kw):
    return LoadableController(*a, **kw)

