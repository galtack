My departure from the GalTacK project
=====================================
Felix Rabe <public@felixrabe.textdriven.com>

Hi all,

I leave the project right now.  Read on for details (aka "my rant").

I didn't even plan to stay that long, but I'm too stubborn :)  But now
I've got other things to work on and I'd be glad if I could hand this
project off to someone who is interested.  So far, noone stepped up.

There are (one to) two reasons for my departure:  (1) I've learned what
I could by developing on this project.  (2) For me, the GalTacK work
ahead is still interesting, but it will be more about getting Galcon
implementation details right than endeavouring into new, fascinating
areas.  This is more time-consuming than I have time to invest.

The most messy work is done (renaming; license matters; and getting the
code I thought was important ported from GPL Galcon (and Galbot) to
GalTacK) and even though GalTacK is nowhere near playable, I've improved
the (Galbot) ground for working on it.  There's a website, a mailing
list, an IRC channel, a playable game (thanks Phil! :o) ), and a few
interested people (see the Galbot website).

I know not all my code is as clean as I initially wished it to be;
especially looking at galtack/net_common.py I'm wondering about my
mental health :) - but hey, it was a start!

I'm grateful to have learned more about:

* Capturing network data using Wireshark
* Using Git (man, that stuff is really hardcore :) )
* Network programming using Twisted
* Using some Python modules I did not know about before
* Respecting other people's work by observing their choice of license
* Doing some basic PR work (need to learn a lot more here)

It also helped me improve a few corners of PyGTK Shell.

If you wish to join the project, those are the things you might have to
familiar with:

* Python (2.4 / 2.5)
* PyGTK (and the fact that PyGTK Shell simplifies it in many ways)
* Twisted (as superficially as I was)
* Galcon (as a player)
* Asciidoc (if you'd like to maintain the current website)
* Git (for fetching from / pushing to repo.or.cz)

I guess several people each knowing only a part of that list and working
together should be able to do more than I was even imagining :)

If you'd like to take over, please send a note to this mailing list, and
if noone opposes, I'll give you proper (non-mob) write access to the Git
repository and redirect galtack.felixrabe.net to your version of the
website.

If you have questions - I'll stay subscribed to this list, so post them
there.

Best wishes, +
Felix

