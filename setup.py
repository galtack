#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK - An unofficial PyGTK client to the Galcon multiplayer game.
"""

# Copyright (C) 2007  Felix Rabe <public@felixrabe.textdriven.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from galtack_client import galtack_version_str

from distutils.core import setup

setup(
        name = "galtack",
        version = galtack_version_str,
        description = "Unofficial PyGtk client for Galcon",
        long_description = __doc__.strip(),
        author = "Felix Rabe",
        author_email = "public@felixrabe.textdriven.com",
        url = "http://felixrabe.textdriven.com/galtack/",
        scripts = ["galtack_client.py"],
        packages = [
            "galtack",
            ],
        classifiers = [
            "License :: OSI Approved :: MIT License",
            ],
      )

