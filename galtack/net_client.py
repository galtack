#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK networking - client-side protocol implementation.

Create a client by copying this example into your program and adding or
removing mixin classes as you wish:

    class GaltackClient(
        galtack.net.GaltackClientRecvCmdLoggerMixin,
        galtack.net.GaltackClientHousekeeperMixin,
        galtack.net.GaltackClientBase,
    ): pass

Do not mix in classes you did not write and do not know completely (like
PyGTK classes), as you might encounter namespace clashes sooner than you
like.  Instead, just instantiate GaltackClient and connect callbacks using
GaltackClientBackCallerMixin (included in the example by means of the
GaltackClientHousekeeperMixin class).
"""
# Copyright (C) 2007 Felix Rabe  <public@felixrabe.textdriven.com>
# Copyright (C) 2007 Michael Carter

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

from galtack.net_client_base            import *
from galtack.net_client_logger          import *
from galtack.net_client_housekeeping    import *
from galtack.net_client_universe        import *

