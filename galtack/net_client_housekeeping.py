#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK networking - client-side protocol housekeeping.
"""
# Copyright (C) 2007 Felix Rabe  <public@felixrabe.textdriven.com>
# Copyright (C) 2007 Michael Carter

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

import sys
import time
from twisted.internet               import defer, reactor
from galtack.net_client_base        import *


class GaltackClientAckExpectingMixin(GaltackClientPrependSeqNrMixin,
                                     GaltackClientBackCallerMixin):
    """
    GaltackClient that expects an [ACK] from the server for each to-be-sent
    command asking for one.
    """
    
    def __init__(self, *a, **kw):
        super(GaltackClientAckExpectingMixin, self).__init__(*a, **kw)
        self.__awaiting_ack = {}
        self.register_command_callback("[ACK]", self.__cmd_ack)

    def __resend(self, seq_nr):
        command, deferred = self.__awaiting_ack[int(seq_nr)]
        self.send_commands(command, prepend_seq_nr = False)
    
    def _pre_send_command_hook(self, command, **kw):
        Cls = GaltackClientAckExpectingMixin
        command = super(Cls, self)._pre_send_command_hook(command, **kw)
        seq_nr, want_ack = map(int, command[:2])
        
        if seq_nr in self.__awaiting_ack:  # we are resending
            del self.__awaiting_ack[seq_nr]
        
        if want_ack:
            deferred = reactor.callLater(1, self.__resend, seq_nr)
            self.__awaiting_ack[seq_nr] = (command, deferred)
        
        return command
        
    def __cmd_ack(self, command):
        seq_nr = int(command[3])
        command, deferred = self.__awaiting_ack.pop(seq_nr, (None, None))
        if deferred is not None:
            deferred.cancel()


class GaltackClientPingSenderMixin(GaltackClientAckSenderMixin):
    """
    GaltackClient that sends out [PING] commands after a while.
    
    This is to make sure the server doesn't timeout the connection because
    we never sent or [ACK]'ed something.
    """
    
    def __init__(self, *a, **kw):
        super(GaltackClientPingSenderMixin, self).__init__(*a, **kw)
        self.__create_ping_delayed()
    
    def __create_ping_delayed(self):
        if hasattr(self, "_GaltackClientPingSenderMixin__ping_delayed"):
            if self.__ping_delayed.active():
                self.__ping_delayed.reset(5)
                return
        self.__ping_delayed = reactor.callLater(5, self.__send_ping)
    
    def _pre_send_commands_hook(self, *commands, **kw):
        sup = super(GaltackClientPingSenderMixin, self)
        commands = sup._pre_send_commands_hook(*commands, **kw)
        self.__create_ping_delayed()
        return commands
    
    def __send_ping(self):
        if self._GaltackClientSendCmdBaseMixin__can_send_commands:
            self.send_commands((1, "[PING]"))
            self.__create_ping_delayed()


class GaltackClientHousekeeperMixin(
        GaltackClientAckExpectingMixin,
        GaltackClientPingSenderMixin,
        GaltackClientAckSenderMixin,
        GaltackClientSendCmdBaseMixin,
    ):
    """
    GaltackClient that interacts properly with a Galcon server.
    
    It takes the responsibility for setting up and shutting down the
    connection.
    
    It (implicitly) defines an imaginary server-side command name
    "[CLOSE]-[ACK]" to which callbacks with the following signature may be
    registered:
        def callback()  # *no* argument
    It gets called whenever a logout could be completed successfully.
    
    Alternatively, you may connect to the deferred returned by logout(),
    which receives None as an argument.
    """
    
    class LogoutAborted(Exception): pass
    
    def __init__(self, *a, **kw):
        super(GaltackClientHousekeeperMixin, self).__init__(*a, **kw)
        self.register_command_callback("version", self.__cmd_version)
        self.__logout_deferred = None
    
    def startProtocol(self):
        super(GaltackClientHousekeeperMixin, self).startProtocol()
        self.send_commands(
            (1, "[RESET]"),
            (1, "[RESET]"),
            (1, "version", "0.18.0"),
        )
    
    def __cmd_version(self, command):
        self.send_commands((1, "login"))
    
    def logout(self):
        self.send_commands(
            (1, "logout"),
            (1, "[PING]"),
        )
        self.register_command_callback("[CLOSE]", self.__cmd_close)
        self.__logout_deferred = defer.Deferred()
        self.__logout_abort = reactor.callLater(10, self.abort_logout)
        return self.__logout_deferred
    
    def abort_logout(self):
        self.__logout_deferred.errback(self.LogoutAborted())

    def __cmd_close(self, command):
        self.__logout_abort.cancel()
        self.unregister_command_callback("[CLOSE]", self.__cmd_close)
        commands = self.send_commands(
            (1, "[CLOSE]"),
        )
        self.__awaiting_ack_for_seq_nr = commands[0][0]
        self.register_command_callback("[ACK]", self.__cmd_close_ack)

    def __cmd_close_ack(self, command):
        if int(command[3]) == self.__awaiting_ack_for_seq_nr:
            self.unregister_command_callback("[ACK]", self.__cmd_close_ack)
            del self.__awaiting_ack_for_seq_nr
            self.__logout_deferred.callback(None)
            self.call_command_callbacks("[CLOSE]-[ACK]")

