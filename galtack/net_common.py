#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK networking - code common to all the GalTacK networking.

Currently this consists of data structures.
"""
# Copyright (C) 2007 Felix Rabe  <public@felixrabe.textdriven.com>
# Copyright (C) 2007 Michael Carter

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

import copy


class TableRowData(object):
    """
    Base class for rows of tables.
    
    Actually, it is very easy to use.  Take the *Info classes further down
    this file as a starting point.
    """
    
    # COL_SPEC example to document the structure - override in derived
    # classes:
    #   (You can use TableRowData.build_col_spec_from_string() to build
    #    COL_SPEC and COL_SPEC_DICT automatically.)
    COL_SPEC = [
        {
            "name":     "user_name",
            "type":     str,
            "caption":  "User",
            "visible":  True,
            "default":  "No Name (default, optional)",
        },
        {
            "name":     "user_password",
            "type":     str,
            "caption":  "Password",
            "visible":  False,
            "default":  "",  # unset
        },
        {
            "name":     "logged_in",
            "type":     bool,
            "caption":  "Logged In?",
            "visible":  False,
            "default":  False,
        }
        # ...
    ]
    
    # COL_SPEC_DICT example to document the structure - override in derived
    # classes:
    #   (You can use TableRowData.build_col_spec_dict() to build
    #    COL_SPEC_DICT automatically.)
    COL_SPEC_DICT = {
        "user_name":        COL_SPEC[0],
        "user_password":    COL_SPEC[1],
        "logged_in":        COL_SPEC[2],
        # ...
    }
    
    def __init__(self, *args, **kwargs):
        """
        Initialize in a user-friendly way.
        
        Examples:
            TableRowData("Peter").user_name == "Peter"
            TableRowData(user_name = "Peter")["user_name"] == "Peter"
        """
        self.__data = {}
        for i, arg in enumerate(args):
            spec = self.COL_SPEC[i]
            self.__data[spec["name"]] = spec["type"](arg)
        for spec in self.COL_SPEC:
            n = spec["name"]
            t = spec["type"]
            if n in kwargs:
                self.__data[n] = t(kwargs.pop(n))
            elif n not in self.__data:
                self.__data[n] = t(spec["default"])
        super(TableRowData, self).__init__(self, **kwargs)
    
    def __copy__(self):
        """
        Implement copy.copy(TableRowData_instance) properly.
        """
        row_data = self.__class__()
        for key, value in self.__data.iteritems():
            row_data[key] = copy.copy(value)
        return row_data
    
    @staticmethod
    def build_col_spec_from_string(string):
        """
        Build COL_SPEC and COL_SPEC_DICT by parsing a string.
        
        Return: (COL_SPEC, COL_SPEC_DICT)
        
        Example:
            TableRowData.build_col_spec_from_string(
                '''
                user_name;     str;  User;    No Name (default, optional)
                user_password; str;  (Password)
                logged_in;     bool; (Logged In?); False
                '''
            ) == (TableRowData.COL_SPEC, TableRowData.COL_SPEC_DICT)
        """
        lines = string.strip().split("\n")
        lines = map(lambda line:
                        map( lambda i: i.strip(), line.split(";") ),
                    lines)
        col_spec = []
        for line in lines:
            # "name"
            name        = line[0]
            # "type"
            type        = __builtins__[line[1]]
            # "caption"
            caption     = line[2]
            # "visible"
            visible     = True
            if caption[0] == "(" and caption[-1] == ")":
                caption = caption[1:-1]
                visible = False
            # "default"
            default     = type()
            if len(line) > 3:
                if type == str:
                    default = line[-1]
                else:
                    default = type(eval(line[-1]))
            spec_dict = {
                "name":     name,
                "type":     type,
                "caption":  caption,
                "visible":  visible,
                "default":  default,
            }
            col_spec.append(spec_dict)
        return (col_spec, TableRowData.build_col_spec_dict(col_spec))
    
    @staticmethod
    def build_col_spec_dict(col_spec):
        """
        Build COL_SPEC_DICT from COL_SPEC.
        
        Return: COL_SPEC_DICT
        
        Example:
            TableRowData.build_col_spec_dict(
                TableRowData.COL_SPEC
            ) == TableRowData.COL_SPEC_DICT
        """
        col_spec_dict = {}
        for spec in col_spec:
            col_spec_dict[spec["name"]] = spec
        return col_spec_dict
    
    def get_data_tuple(self):
        """
        Get a tuple containing all the data.
        """
        tup = []
        for spec in self.COL_SPEC:
            tup.append(self.__data[spec["name"]])
        return tuple(tup)
    
    def get_visible_data_tuple(self):
        """
        Get a tuple containing all the visible data.
        """
        tup = []
        for i, x in enumerate(self.get_tuple()):
            if self.COL_SPEC[i]["visible"] == True:
                tup.append(x)
        return tuple(tup)
    
    @classmethod
    def get_col_types(cls):
        """
        Get a tuple containing all the data types.
        """
        return tuple(s["type"] for s in cls.COL_SPEC)
    
    @classmethod
    def get_visible_col_types(cls):
        """
        Get a tuple containing all the visible data types.
        """
        tup = []
        for i, x in enumerate(cls.get_types()):
            if cls.COL_SPEC[i]["visible"] == True:
                tup.append(x)
        return tuple(tup)
    
    def __getitem__(self, name):
        """
        Make self["user_name"] and self[0] work.
        """
        i = None
        try:
            i = int(name)
        except: pass
        if i is not None:
            spec = self.COL_SPEC[i]
            return self.__data[spec["name"]]
        try:
            return self.__data[name]
        except:
            raise AttributeError, name
    
    def __getattr__(self, name):
        """
        Make self.user_name work.
        """
        return self.__getitem__(name)
    
    def _get_field_spec(self, name_or_index_or_spec):
        """
        Return the COL_SPEC entry for the given name or index.
        
        You can pass a COL_SPEC entry as well if you called
        self._get_field_spec before.
        """
        if isinstance(name_or_index_or_spec, dict):
            return name_or_index_or_spec  # spec
        i = None
        try:
            i = int(name_or_index_or_spec)
        except: pass
        if i is not None:
            return self.COL_SPEC[i]  # index
        name = name_or_index_or_spec
        if name not in self.__data:
            raise AttributeError, "unknown field name '%s'" % name
        return self.COL_SPEC_DICT[name]  # name
    
    def __setitem__(self, spec, value):
        """
        Implement self["password"] (or self[1]) = "my0dear1secret".
        """
        spec = self._get_field_spec(spec)
        self.__data[spec["name"]] = spec["type"](value)
    
    def __setattr__(self, name, value):
        """
        Make self.password = 'my0dear1secret' work.
        """
        if name == "_TableRowData__data":
            return super(TableRowData, self).__setattr__(name, value)
        return self.__setitem__(name, value)
    
    def format(self, use_caption = True):
        """
        Format this row of data for human eyes.
        """
        lines = []
        n = use_caption and "caption" or "name"
        maxlen = max(len(s[n]) for s in self.COL_SPEC)
        for spec in self.COL_SPEC:
            lines.append(("%%-%us%%r" % (maxlen + 2)) %
                         (spec[n] + ":",
                          self.__data[spec["name"]]))
        lines.append("")
        return "\n".join(lines)


class ServerInfo(TableRowData):
    """
    Information about a GalTacK server.
    """
    
    COL_SPEC, COL_SPEC_DICT = TableRowData.build_col_spec_from_string("""
        bots_ok;        bool;   (Bots Ok)
        allowed;        str;    Ranks
        owner;          str;    Owner
        version;        str;    Version
        num;            int;    Players
        name;           str;    Name
        pwd_protected;  bool;   Protected
        status;         str;    (Status)
        addr;           str;    (IP Address)
        port;           int;    (Port);     9031
        secret;         str;    (Secret Hash)
        passwd;         str;    (Password)
    """)


class UserInfo(TableRowData):
    """
    Information about a GalTacK user.
    """
    
    COL_SPEC, COL_SPEC_DICT = TableRowData.build_col_spec_from_string("""
        email;          str;    Email
        name;           str;    Name
        passwd;         str;    Password
        platform;       str;    Platform;   linux2
        version;        str;    Version;    1.2.0
    """)


class PlayerPlayersInfo(TableRowData):
    """
    Information about a GalTacK player.
    """
    
    COL_SPEC, COL_SPEC_DICT = TableRowData.build_col_spec_from_string("""
        num;                int;    Number
        name;               str;    Name
        junk1;              str;    Junk 1
        junk2;              str;    Junk 2
        state;              str;    State
        junk3;              str;    Junk 3
        junk4;              str;    Junk 4
        color;              str;    Color
    """)


class PlayerStartInfo(TableRowData):
    """
    In-game information about a GalTacK player.
    """
    
    COL_SPEC, COL_SPEC_DICT = TableRowData.build_col_spec_from_string("""
        num;                int;    Number
        name;               str;    Name
        junk1;              str;    Junk 1
        color;              str;    Color
    """)


class OptionB64Info(TableRowData):
    """
    In-game, base64-encoded information about GalTacK "options".
    """
    
    COL_SPEC, COL_SPEC_DICT = TableRowData.build_col_spec_from_string("""
        scale;              float;  Scale
        junk2;              float;  Junk 2
        junk3;              float;  Junk 3
        junk4;              float;  Junk 4
        junk5;              float;  Junk 5
        junk6;              float;  Junk 6
    """)


class PlayerB64Info(TableRowData):
    """
    In-game, base64-encoded information about a GalTacK player.
    """
    
    COL_SPEC, COL_SPEC_DICT = TableRowData.build_col_spec_from_string("""
        num;                int;    Number
        name;               str;    Name
        junk1;              str;    Junk 1
        color;              str;    Color
    """)


class PlanetB64Info(TableRowData):
    """
    (In-game) information about a GalTacK planet.
    """
    
    COL_SPEC, COL_SPEC_DICT = TableRowData.build_col_spec_from_string("""
        num;            int;    Number;             0
        x;              int;    X Position;         0
        y;              int;    Y Position;         0
        owner_num;      int;    Owner (Number);     0
        prod;           int;    Production;         0
        troops;         int;    Troops;             0
        junk1;          str;    Junk 1;             0
        junk2;          str;    Junk 2;             0
        junk3;          str;    Junk 3;             0
        junk4;          str;    Junk 4;             0
        junk5;          str;    Junk 5;             0
        junk6;          str;    Junk 6;             0
        junk7;          str;    Junk 7;             0
        junk8;          str;    Junk 8;             0
        junk9;          str;    Junk 9;             0
        radius;         float;  (Radius);           0.0
        level_scale;    float;  (Level Scale);      1.0
        dispx;          int;    (Displayed X Pos.); -1
        dispy;          int;    (Displayed Y Pos.); -1
    """)

    def __init__(self, *a, **kw):
        super(PlanetB64Info, self).__init__(*a, **kw)
        self.prod = self.prod  # triggers __setitem__ (from __setattr__)
    
    def __setitem__(self, spec, value):
        spec = self._get_field_spec(spec)
        super(PlanetB64Info, self).__setitem__(spec, value)
        name = spec["name"]
        if name == "prod" or name == "level_scale":
            self.radius = int( (12 + self.prod / 10) * self.level_scale )

