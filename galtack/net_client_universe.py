#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK networking - client-side universe bookkeeping.
"""
# Copyright (C) 2007 Felix Rabe  <public@felixrabe.textdriven.com>
# Copyright (C) 2007 Michael Carter

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

import base64
import zlib

from galtack.net_client_base import *


class GaltackClientPlayerTrackerMixin(GaltackClientBackCallerMixin):
    """
    GaltackClient that keeps track of players.
    """
    
    def __init__(self, *a, **kw):
        super(GaltackClientPlayerTrackerMixin, self).__init__(*a, **kw)
        self.player_info_dict = {}
        self.player_ingame_info_dict = {}
        self.register_command_callback("players",   self.__cmd_players)
        self.register_command_callback("start",     self.__cmd_start)
        self.register_command_callback("stop",      self.__cmd_stop)

    def __cmd_players(self, command):
        recv_players = command[3:]
        self.player_info_dict = {}
        for player in recv_players:
            s = player.split(',')
            self.player_info_dict[s[0]] = PlayerPlayersInfo(*s)
    
    def __cmd_start(self, command):
        self.player_ingame_info_dict = {}
        for player in command[3].split(';'):
            s = player.split(',')
            self.player_ingame_info_dict[s[0]] = PlayerStartInfo(*s)
    
    def __cmd_stop(self, command):
        self.player_ingame_info_dict = {}


class GaltackClientGameTrackerMixin(GaltackClientBackCallerMixin):
    """
    GaltackClient that keeps track of the game state (running or not).
    """
    
    def __init__(self, *a, **kw):
        super(GaltackClientGameTrackerMixin, self).__init__(*a, **kw)
        self.game_running = False
        self.register_command_callback("start",     self.__cmd_start)
        self.register_command_callback("stop",      self.__cmd_stop)
    
    def __cmd_start(self, command):
        self.game_running = True
    
    def __cmd_stop(self, command):
        self.game_running = False


class UniverseB64Data(object):
    """
    Core data structure representing the universe data encoded in Base64,
    and nothing more.
    """
    
    LINE_LEN = {
        "version":  2,
        "options":  7,
        "user":     5,
        "planet":   16,
    }

    def __init__(self, b64string = None):
        self._reset_stuff()
        if b64string is not None:
            self._raw_data = self.__decode(b64string)
        
        self.options = None
        for line in (li.split("\t") for li in self._raw_data.split("\n")):
            if line[0] == "planet":
                self.planets.append(PlanetB64Info(*line[1:]))
                if self.options is not None:
                    self.planets[-1].level_scale = self.options.scale
                continue
            if line[0] == "user":
                self.users.append(PlayerB64Info(*line[1:]))
                continue
            if line[0] == "options":
                self.options = OptionB64Info(*line[1:])
                continue
            if line[0] == "version":
                self.version = int(line[1])
                continue
            if len(line) == 1 and line[0] == "":
                continue
            raise Exception, "unrecognized format"
    
    def _reset_stuff(self):
        self._raw_data          = ""
        self._version_updated   = False
        self.version            = None
        self.options            = None
        self.users              = []
        self.planets            = []
    
    def __copy__(self):
        d = UniverseB64Data()
        d._raw_data         = copy.copy(self._raw_data)
        d._version_updated  = copy.copy(self._version_updated)
        d.version           = copy.copy(self.version)
        d.options           = copy.copy(self.options)
        d.users             = map(copy.copy, self.users)
        d.planets           = map(copy.copy, self.planets)
        return d
    
    def update(self, b64string):
        self._raw_data = self.__decode(b64string)
        planet_index = 0
        for line in (li.split("\t") for li in self._raw_data.split("\n")):
            if len(line) == self.LINE_LEN["planet"]:
                p = self.planets[planet_index]
                for i, field in enumerate(line[1:]):
                    if not field: continue
                    p[i] = field
                planet_index += 1
                continue
            if any(line):
                raise Exception, "unrecognized format %r" % line
    
    @staticmethod
    def __decode(b64string):
        return zlib.decompress(base64.b64decode(b64string))


class GaltackClientUniverseTrackerMixin(GaltackClientBackCallerMixin):
    """
    GaltackClient that keeps its idea of the GalTacK universe up-to-date.
    """
    
    def __init__(self, *a, **kw):
        super(GaltackClientUniverseTrackerMixin, self).__init__(*a, **kw)
        self.u64data = None
        self.register_command_callback("start",     self.__cmd_start)
        self.register_command_callback("delta",     self.__cmd_delta)
        self.register_command_callback("stop",      self.__cmd_stop)
    
    @staticmethod
    def __decode(data):
        return zlib.decompress(base64.b64decode(data))
    
    def __cmd_start(self, command):
        if len(command) < 4: return None
        self.players_start = [l.split(",") for l in command[3].split(";")]
        if len(command) < 6: return None
        self.u64data = UniverseB64Data(command[5])
        return None
        
    def __cmd_delta(self, command):
        if self.u64data is None:
            print "whoops, no u64data"
            return None
        self.u64data.update(command[3])
    
    def __cmd_stop(self, command):
        self.u64data = None

