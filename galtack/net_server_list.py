#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK networking - Galcon server list retrieval.
"""
# Copyright (C) 2007 Felix Rabe  <public@felixrabe.textdriven.com>
# Copyright (C) 2007 Michael Carter

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

import urllib

from twisted.internet import reactor, defer
from twisted.internet.protocol import ClientFactory
from twisted.web import http

from galtack.net_common import *


def get_server_list(user_info):
    """
    Retrieve a list of servers (games).
    """
    deferred = defer.Deferred()
    factory = ServerListClientFactory(deferred, user_info)
    reactor.connectTCP("imitationpickles.org", 80, factory)
    return deferred


class ServerListException(Exception): pass


class ServerListClient(http.HTTPClient):
    """
    Galcon HTTP client for retrieving the list of servers (games) from
    www.imitationpickles.org.
    """

    URL = "/galcon/api.php"
    
    def __init__(self, deferred, user_info):
        self.__deferred     = deferred
        self.__user_info    = user_info
        
    def connectionMade(self):
        url_args = {
            'action':       'list',
            'reg_email':    self.__user_info.email,
            'name':         self.__user_info.name,
            'passwd':       self.__user_info.passwd,
            'platform':     self.__user_info.platform,
            'version':      self.__user_info.version,
        }
        url = self.URL + '?' + urllib.urlencode(url_args.items())
        self.sendCommand('GET', url)
        self.sendHeader('Accept-Encoding', 'identity')
        self.sendHeader('Host', 'www.imitationpickles.org')
        self.sendHeader('Connection', 'close')
        self.sendHeader('User-agent', 'Python-urllib/2.4')
        self.endHeaders()
        
    def handleResponse(self, response):
        try:
            lines = response.split('\n')
            if lines[0].startswith("ERROR:"):
                raise ServerListException, lines[0].split(None, 1)[1]
            current_version = lines[0].split()[1].split(".")
            server_lines = lines[3:]
            server_info_list = []
            for server_line in server_lines:
                if server_line == "": continue
                owner_allowed, name, version, more = server_line.split('|')
                owner, allowed = owner_allowed.split('\t')
                num, pwd_protected, addr, secret, status = more.split('\t')
                server_info_list.append(ServerInfo(
                    owner           = owner,
                    allowed         = allowed,
                    name            = name,
                    version         = version,
                    num             = int(num),
                    pwd_protected   = bool(int(pwd_protected)),
                    addr            = addr,
                    secret          = secret,
                    status          = status,
                    # bots_ok         = owner.lower() == 'korin',
                    bots_ok         = (status == "open"),
                ))
            self.__deferred.callback((current_version, server_info_list))
        except:
            self.__deferred.errback()


class ServerListClientFactory(ClientFactory):
    """
    Galcon HTTP client factory for retrieving the list of servers (games)
    from www.imitationpickles.org.
    
    Usage:
    >>> deferred = defer.Deferred()
    >>> user_info = UserInfo(email, name, passwd, platform, version)
    >>> factory = ServerListClientFactory(deferred, user_info)
    >>> reactor.connectTCP("imitationpickles.org", 80, factory)
    
    deferred callback / errback signatures:
        def cb((current_version, server_info_list)) # return None
        def eb(failure)                             # return None
    """

    protocol = ServerListClient
    
    def __init__(self, deferred, user_info):
        self.__deferred     = deferred
        self.__user_info    = user_info
    
    def buildProtocol(self, addr):
        return self.protocol(self.__deferred, self.__user_info)


if __name__ == "__main__":
    import sys
    print "Running this Python module is not yet supported."
    sys.exit(1)

