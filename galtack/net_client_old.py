#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Galcon networking - old client protocol implementation.

This file is only used for reference and can safely be deleted.
"""
# Copyright (C) 2007 Felix Rabe  <public@felixrabe.textdriven.com>
# Copyright (C) 2007 Michael Carter

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.



### FR OLD CODE

class GalconUDPProtocol(DatagramProtocol):
    header = "[HEADER]\tpasswd\t%s\n[HEADER]\tsecret\t%s\n[HEADER]\tname\t%s\n"
    def __init__(self, client, address, port, server_name, digest, server_password, name, user_password):
        self.server_name = server_name
        self.server_password = server_password
        self.user_password = user_password
        self.digest = digest
        self.name = name
        self.client = client
        self.address = address
        self.port = port
        # self.received = []
        self.awaiting_ack = dict()
        self.id = 3
        self.players = dict()
        self.game = False
        self.universe = None
        self.players_details = None
        
    def decode(self, data):
        return zlib.decompress(base64.decodestring(data))
    
    def startProtocol(self):
        self.transport.connect(self.address, self.port)
        self.send_commands([(1, "[RESET]"), (1, "[RESET]"), (1, "version", "0.18.0")])

    def resend(self, id):
        cmd, timer = self.awaiting_ack[int(id)]
        self.send_command(cmd)
        
    def send_commands(self, cmds):
        if not cmds:
            return
#        print "PRESEND: %s" % (cmds,)
        id_cmds = []
        for cmd in cmds:
            
            if cmd[0] not in self.awaiting_ack:
                self.id += 1
                cmd = (self.id,) + cmd
            else:
                self.awaiting_ack.pop(cmd[0])
                
            if cmd[1] == 1:
                d = reactor.callLater(1, self.resend, cmd[0])
                self.awaiting_ack[cmd[0]] = cmd, d
                    
            id_cmds.append(cmd)
        cmds = id_cmds
        packet = self.header % (self.server_password, self.digest, self.name) + '\n'.join(['\t'.join([str(i) for i in cmd]) for cmd in cmds]) + '\n'
        self.transport.write(packet)
#        print "SEND: %s" % packet.replace("\n", "\\n").replace("\t", "\\t")
        
    def send_command(self, cmd):
        self.send_commands([cmd])
                
    def datagramReceived(self, datagram, host):
#        print "RECV: %s" % datagram
        cmds = [ line.split('\t', 3) for line in datagram.split('\n') ]
        cmds = [ i for i in cmds if len(i) > 1 ]
#        print "RECV: %s" % (cmds,)
        to_ack = [ cmd[0] for cmd in cmds if cmd[1] == '1' ]
        self.send_commands([(0, "[ACK]", id) for id in to_ack ])
        
        for cmd in cmds:
            # if cmd[0] in self.received:
            #     continue
            # self.received.append(cmd[0])
            cmd[2] = cmd[2].replace("[", "_").replace("]", "_")
            rest = ()
            if len(cmd) == 4:
                rest = cmd[3].split('\t')
            f = getattr(self, "recv_%s" % cmd[2], self.default)
            if f == self.default:
                f(cmd[2], rest)
            else: 
                f(*rest)
    
    def default(self, cmd, *data):
        print "DEFAULT HANDLER: COMMAND [%s]: %s" % (cmd, data,)
        
    def recv_version(self, data):
#        print "GOT VERSION"
        self.send_command((1, "login"))
        
    def recv_topic(self, topic):
#        print "TOPIC IS %s" % topic
        self.client.galcon_recv_topic(self.server_name, topic)
        
    def recv__ACK_(self, id):
#        print "ACK: %"
        cmd, timer = self.awaiting_ack.pop(int(id), (None, None))
        if timer:
            timer.cancel()
            
        
    def recv_message(self, sender, msg):
        self.client.galcon_recv_message(self.server_name, sender, msg)

    def recv_players(self, *players):
        if self.players:
            self.players = {}
        for player in players:
            num, name, junk1, junk2, state, junk3, junk4, color = player.split(',')
            self.players[name] = (name, state, color)
        self.client.galcon_recv_players(self.server_name, players)

    def recv__PING_(self):
        self.client.galcon_recv_ping(self.server_name)

    def recv__CLOSE_(self):
        self.client.galcon_recv_close(self.server_name)
       
    
    def recv_mfleet(self, *args):
        self.client.galcon_recv_mfleet(self.server_name, *args)

    def recv_redirect(self, *args):
        self.client.galcon_recv_redirect(self.server_name, *args)

        
    def recv_stop(self):
        self.game = False
        self.players_details = None
        self.universe = None
        self.client.galcon_recv_stop(self.server_name)
        
    def recv_start(self, players, time, universe):
        self.game = True
        self.players_details = {}
        self.universe = { 'players': {}, 'planets': {}, 'options': None }
        for player in players.split(';'):
            num, name, junk, color = player.split(',')
            self.players_details[num] = name, num, color
        
        universe_data = self.decode(universe)
        lines = universe_data.split('\n')
        self.universe['options'] = lines[1].split('\t')
        delta = self.parse_state(lines[2:])
        self.universe['players'].update(delta['players'])
        self.universe['planets'].update(delta['planets'])
        self.client.galcon_recv_start(self.server_name, self.universe)
        
    def parse_state(self, state_lines, is_delta=False):
        delta = {'players': {}, 'planets': {} }
        for i, line in enumerate(state_lines):
            if is_delta:
                type = 'planet'
                junk, rest = line.split('\t', 1)
            else:
                type, rest = line.split('\t', 1)
                
            if type == 'user':
                id, rest = rest.split('\t', 1)
                delta['players'][id] = {
                    'name': self.players_details[id][0],
                    'id': self.players_details[id][1],
                    'color': self.players_details[id][2], }
            elif type == 'planet':
                id, x, y, owner_id, prod, troops, junk = rest.split('\t', 6)                
                if is_delta:
                    if not rest.strip():
                        continue                    
                    id = str(i+1)
                    p = self.universe['planets'][id]
                    x,y,prod = p['x'], p['y'], p['prod']
                    if not owner_id:
                        owner_id = p['owner_id']
                        
                delta['planets'][id] = {
                    'id': id,
                    'x': x,
                    'y': y,
                    'owner_id': owner_id,
                    'prod': prod,
                    'troops': troops
                }
        return delta
        
    def recv_delta(self, delta):
        universe_delta = self.decode(delta)
        planets = universe_delta.split('\n')[-len(self.universe['planets']):]
        delta = self.parse_state(planets, True)
        self.universe['planets'].update(delta['planets'])
        self.client.galcon_recv_delta(self.server_name, self.universe)
    
    def recv_options(self, option_string):
        print "option string: %r" % option_string

