#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK networking - basic client protocol classes.
"""
# Copyright (C) 2007 Felix Rabe  <public@felixrabe.textdriven.com>
# Copyright (C) 2007 Michael Carter

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

from twisted.internet.protocol      import DatagramProtocol
from galtack.net_common             import *


class GaltackClientBase(DatagramProtocol, object):
    """
    GalTacK network client base class (Galcon-compatible).
    """

    def __init__(self, options, user_info, server_info, *a, **kw):
        """
        Initialize.
        
        The arguments are a galtack.net_common.UserInfo and a
        galtack.net_common.ServerInfo instance.
        """
        super(GaltackClientBase, self).__init__(*a, **kw)
        self._options       = options
        self._user_info     = user_info
        self._server_info   = server_info

    def startProtocol(self):
        """
        Called by twisted when a transport is connected to this protocol.
        """
        self.transport.connect(self._server_info.addr,
                               self._server_info.port)
        
    def datagramReceived(self, datagram, host):
        """
        Called by twisted when a datagram is received.
        """
        return self._commands_received(self, datagram)
        
    def _commands_received(self, commands):
        """
        Called with the commands received.
        
        Fill this method with meaning in the mixin classes -- and only
        there, please.
        """
    
    @classmethod
    def _modify_option_parser(cls, option_parser):
        """
        Modify the specified option_parser (an optparse.OptionParser).
        
        This can be used e.g. to add mixin-specific options.
        """
        return option_parser
    
    @classmethod
    def _mkopt(cls, option_name):
        """
        Helper method for _modify_option_parser().
        """
        return "_%s__%s" % (cls.__name__, option_name)
    
    def _getopt(self, option_name, kw, kw_default = -1):
        """
        Helper method for __init__() to set an option value.
        """
        value = kw.pop(option_name, kw_default)
        if value is kw_default:
            name = "_%s__%s" % (self.__class__.__name__, option_name)
            value = getattr(self._options, name)
        return value


class GaltackClientSendCmdBaseMixin(GaltackClientBase):
    """
    GaltackClient with a send_commands() method.
    
    This only provides the general infrastructure for sending GalTacK
    commands.  Sequence number adding and [ACK] handling is done by other
    mixin classes.
    """
    
    _HEADER = ("[HEADER]\tpasswd\t%s\n" +
               "[HEADER]\tsecret\t%s\n" +
               "[HEADER]\tname\t%s\n")
    
    def __init__(self, *a, **kw):
        super(GaltackClientSendCmdBaseMixin, self).__init__(*a, **kw)
        self.__can_send_commands = False
    
    def startProtocol(self):
        super(GaltackClientSendCmdBaseMixin, self).startProtocol()
        self.__can_send_commands = True
    
    def stopProtocol(self):
        super(GaltackClientSendCmdBaseMixin, self).stopProtocol()
        self.__can_send_commands = False
    
    def _pre_send_command_hook(self, command, **kw):
        """
        Process a command before sending, e.g. by prepending the sequence
        number.
        
        The 'command' argument is a tuple created by something like
        tuple(cmd_line.split("\t")).
        
        Remember to call this method using super() from subclasses
        overriding this hook, and to return its (possibly modified) return
        value.  Even though this method is empty.
        """
        return command
    
    def _pre_send_commands_hook(self, *commands, **kw):
        """
        Process the specified commands before sending.
        
        Remember to call this method using super() from subclasses
        overriding this hook, and to return its (possibly modified) return
        value.
        """
        try:
            f = lambda cmd: self._pre_send_command_hook(cmd, **kw)
            return map(f, map(tuple, commands))
        except:
            import traceback
            traceback.print_exc()
        return commands
    
    def _post_send_datagram_hook(self, datagram, **kw):
        """
        Post-process the sent datagram.
        """
        return datagram
    
    def send_commands(self, *commands, **kw):
        """
        Send the specified commands.
        
        Any keyword arguments given are passed on to the hooks.
        """
        if not self.__can_send_commands:
            raise Warning, ("unable to send commands since " +
                            "the protocol has not been started yet")
        if not commands:  # no cmds to send, e.g. when they were generated
            return
        
        header = self._HEADER % (self._server_info.passwd,
                                 self._server_info.secret,
                                 self._user_info.name)
        
        cmds = self._pre_send_commands_hook(*commands, **kw)
        cmds_str = "\n".join("\t".join(map(str, c)) for c in cmds)
        
        datagram = header + cmds_str + '\n'
        self.transport.write(datagram)
        
        self._post_send_datagram_hook(datagram, **kw)
        
        return cmds


class GaltackClientIgnoreResentCmdsMixin(GaltackClientBase):
    """
    GaltackClient that ignores resent commands from the server.
    
    The server might resend a command since it didn't get our [ACK]
    response (done manually or using GaltackClientAckSenderMixin), in
    which case only the first instance of that command is important to us
    for post-[ACK]-sending-processing.
    
    As it gets increasingly expensive to keep track of really *all*
    commands (or their sequence numbers) that got ever sent to us, this
    class by default keeps a ring buffer of the sequence numbers of the
    last 100 commands received.
    
    If you want to be really strict and don't care about performance at
    all, pass __init__(ignore_resent_cmds_ring_bufsize = None).
    """
    
    def __init__(self, *a, **kw):
        self.__command_callbacks = {}
        self.__seq_nr_buffer = []
        bufsize = kw.pop("ignore_resent_cmds_ring_bufsize", 100)
        self.__seq_nr_buffer_size = bufsize
        super(GaltackClientIgnoreResentCmdsMixin, self).__init__(*a, **kw)
    
    def __seq_nr_buffer_truncate(self):
        """
        Truncate self.__seq_nr_buffer to self.__seq_nr_buffer_size.
        """
        bufsize = self.__seq_nr_buffer_size
        if bufsize is None: return None
        self.__seq_nr_buffer = self.__seq_nr_buffer[-bufsize:]
    
    def datagramReceived(self, datagram, host):
        commands = tuple( tuple(line.split("\t"))
                          for line in datagram.split("\n") )
        # drop empty lines
        commands = filter(lambda c: c != ("",), commands)
        new_commands = []
        for command in commands:
            try:    seq_nr, want_ack = map(int, command[:2])
            except: continue  # in case the server would send [HEADER]
            if seq_nr == 1:  # numbers got reset, we reset too
                self.__seq_nr_buffer = []
            if seq_nr in self.__seq_nr_buffer:  # command already received
                continue
            new_commands.append(command)
            self.__seq_nr_buffer.append(seq_nr)
        self.__seq_nr_buffer_truncate()
        return self._commands_received("\n".join( map("\t".join,
                                                      new_commands) ))


class GaltackClientBackCallerMixin(GaltackClientIgnoreResentCmdsMixin):
    """
    GaltackClient that allows callbacks to be registered to respond to
    certain GalTacK commands received from a server.
    """
    
    def __init__(self, *a, **kw):
        self.__command_callbacks = {}
        super(GaltackClientBackCallerMixin, self).__init__(*a, **kw)
    
    def register_command_callback(self, cmd_name = None, callback = None):
        """
        Register a command with a callback.
        
        Example:
            galtack_client.register_command_callback("message",
                                                     cmd_message)
        
        Callbacks registered to no command name (cmd_name = None) are
        implicitly registered to all command names (individually).
        Example:
            galtack_client.register_command_callback(callback =
                                                     cmd_catchall)
        
        Callbacks registered to cmd_name = False are registered to the
        whole command *chunks* that arrive in a UDP packet.
        
        The signature of a callback function (add self for a method) looks
        like this:
            def callback(command)
        The 'command' argument is a tuple created by something like
        tuple(cmd_line.split("\t")), or, in the case of a callback
        registered to cmd_name = False, a tuple of those tuples.
        """
        # command argument is optional - callback is not:
        if callback is None:
            raise TypeError, ("register_command_callback() requires the " +
                              "callback argument")
        callback_list = self.__command_callbacks.get(cmd_name, [])
        callback_list.append(callback)
        self.__command_callbacks[cmd_name] = callback_list
    
    def unregister_command_callback(self, cmd_name = None,
                                          callback = None):
        """
        Unregister a callback.
        
        Works like register_command_callback(), just reversed.
        """
        callback_list = self.__command_callbacks[cmd_name]
        # Do it this way to remove the _last_ (added) callback:
        callback_list.reverse()
        callback_list.remove(callback)
        callback_list.reverse()
    
    def call_command_callbacks(self, cmd_name = None, *a, **kw):
        """
        Run the callbacks registered to cmd_name with the given arguments.
        
        Calling this method can be used to simulate commands received from
        the server.
        
        Alternatively, you can use it to "invent" commands.
        """
        cb_list = []
        if kw.pop("run_common", False):
            cb_list = self.__command_callbacks.get(None, [])
        cb_list += self.__command_callbacks.get(cmd_name, [])
        for callback in cb_list:
            callback(*a, **kw)
    
    def _commands_received(self, commands):
        """
        Call the registered callbacks for the received commands.
        """
        commands = tuple( tuple(line.split("\t"))
                          for line in commands.split("\n") )
        commands = filter(lambda c: c, commands)  # drop empty lines
        
        self.call_command_callbacks(False, commands)
        
        common_callbacks = self.__command_callbacks.get(None, [])
        for command in commands:
            try:    map(int, command[:2])
            except: continue  # in case the server would send [HEADER]
            name = command[2]
            self.call_command_callbacks(name, command, run_common = True)
        
        # Call parent class' _commands_received() method:
        Cls = GaltackClientBackCallerMixin
        return super(Cls, self)._commands_received(commands)


class GaltackClientPrependSeqNrMixin(GaltackClientBackCallerMixin,
                                     GaltackClientSendCmdBaseMixin):
    """
    GaltackClient that adds the sequence number to each command before
    sending.
    """
    
    def __init__(self, *a, **kw):
        super(GaltackClientPrependSeqNrMixin, self).__init__(*a, **kw)
        self.__seq_nr = 0
        self.register_command_callback("[RESET]", self.__cmd_reset)
    
    def _pre_send_command_hook(self, command, **kw):
        """
        To disable this hook, call:
            send_command(..., prepend_seq_nr = False)
        """
        prepend_seq_nr = kw.pop("prepend_seq_nr", True)
        Cls = GaltackClientPrependSeqNrMixin
        command = super(Cls, self)._pre_send_command_hook(command, **kw)
        if prepend_seq_nr:
            self.__seq_nr += 1
            command = (self.__seq_nr,) + command
        return command
    
    def __cmd_reset(self, command):
        if int(command[0]) == 1:
            self.__seq_nr = 0


class GaltackClientAckSenderMixin(GaltackClientPrependSeqNrMixin,
                                  GaltackClientIgnoreResentCmdsMixin):
    """
    GaltackClient that sends out [ACK] commands when needed.
    
    This intercepts datagrams *before* GaltackClientIgnoreResentCmdsMixin
    gets a chance to process them.
    """
    
    def __init__(self, *a, **kw):
        super(GaltackClientAckSenderMixin, self).__init__(*a, **kw)
        self.__debug_log = file("/tmp/acksend.log", "w")
    
    def datagramReceived(self, datagram, host):
        self.__debug_log.write("\n* datagramReceived:\n\n%s\n\n" % datagram)
        Cls = GaltackClientAckSenderMixin
        super(Cls, self).datagramReceived(datagram, host)
        self.__debug_log.write("\n* Received\n")
        commands = tuple( tuple(line.split("\t"))
                          for line in datagram.split("\n") )
        seq_nr_list = []
        for command in commands:
            try:
                seq_nr, want_ack = map(int, command[0:2])
            except: continue  # in case the server would send [HEADER]
            if not want_ack: continue
            seq_nr_list.append(seq_nr)
        self.send_commands(*((0, "[ACK]", sn) for sn in seq_nr_list))

