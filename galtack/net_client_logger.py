#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK networking - client-side logging.
"""
# Copyright (C) 2007 Felix Rabe  <public@felixrabe.textdriven.com>
# Copyright (C) 2007 Michael Carter

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

import time
from galtack.net_client_base import *


class GaltackClientCmdLoggerBaseMixin(GaltackClientBase):
    """
    GaltackClient base mixin for classes implementing traffic dumping.
    """
    
    def __init__(self, *a, **kw):
        super(GaltackClientCmdLoggerBaseMixin, self).__init__(*a, **kw)
        self._command_log_file = self._getopt("command_log_file", kw)
        if isinstance(self._command_log_file, basestring):
            self._command_log_file = file(self._command_log_file, "wb")
    
    @classmethod
    def _modify_option_parser(cls, option_parser):
        C = GaltackClientCmdLoggerBaseMixin
        option_parser = super(C, cls)._modify_option_parser(option_parser)
        option_parser.add_option("-l", "--command-log-file",
                                 dest = cls._mkopt("command_log_file"),
                                 metavar = "FILE", default = None,
                                 help = "log Galtack commands to a file")
        return option_parser


class GaltackClientRecvCmdLoggerMixin(GaltackClientCmdLoggerBaseMixin):
    """
    GaltackClient that dumps received commands to self.command_dump_file.
    """
    
    def datagramReceived(self, datagram, host):
        """
        Override datagramReceived instead of _commands_received as we want
        to log *everything* (resent commands as well).
        """
        f = self._command_log_file
        if f is not None:
            f.write("\n*** COMMANDS ** SERVER ** %f ***\n" % time.time())
            f.write(datagram)
        # Call parent class' datagramReceived method:
        Cls = GaltackClientRecvCmdLoggerMixin
        return super(Cls, self).datagramReceived(datagram, host)


class GaltackClientSendCmdLoggerMixin(GaltackClientCmdLoggerBaseMixin,
                                      GaltackClientSendCmdBaseMixin):
    """
    GaltackClient that dumps sent commands to self.command_dump_file.
    """

    def _post_send_datagram_hook(self, datagram, **kw):
        """
        Post-process the sent datagram.
        """
        f = self._command_log_file
        if f is not None:
            f.write("\n*** COMMANDS ** CLIENT ** %f ***\n" % time.time())
            f.write(datagram)
        # Call parent class' datagramReceived method:
        Cls = GaltackClientSendCmdLoggerMixin
        return super(Cls, self)._post_send_datagram_hook(datagram, **kw)

