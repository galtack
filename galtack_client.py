#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GalTacK - An unofficial PyGTK client to the Galcon multiplayer game.
"""
# Copyright (C) 2007  Michael Carter
# Copyright (C) 2007  Felix Rabe <public@felixrabe.textdriven.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Recommended line length or text width: 75 characters.

galtack_version = (0,2,0)
galtack_version_str = ".".join(map(str, galtack_version))

import optparse
import math
PI2 = math.pi * 2
import time

from PyGTKShell.Config import _config
_config["main-loop-integrate-twisted"] = True
from PyGTKShell.RawConsole import *
arrow_cursor = gtk.gdk.Cursor(gtk.gdk.TOP_LEFT_ARROW)
watch_cursor = gtk.gdk.Cursor(gtk.gdk.WATCH)

from twisted.internet import reactor

import galtack.net
class GaltackClient(
    galtack.net.GaltackClientRecvCmdLoggerMixin,
    galtack.net.GaltackClientSendCmdLoggerMixin,
    galtack.net.GaltackClientUniverseTrackerMixin,
    galtack.net.GaltackClientHousekeeperMixin,
    galtack.net.GaltackClientBase,
): pass


### CUSTOMIZED STUFF FROM PYGTK SHELL [START]


## RawConsole


class RawConsoleWithIntroMixin(RawConsoleBase):
    """
    Raw console running an example script on initialization.
    
    Customized for GalTacK.
    """

    def __init__(self, *a, **kw):
        super(RawConsoleWithIntroMixin, self).__init__(*a, **kw)
        buf = self.code_view.get_buffer()
        msg = a_("Press F5 or Ctrl+E to execute this code.")
        buf('# -*- coding: utf-8 -*-\n' +
            '# %s\n' % msg +
            'from galtack_client import *\n' +
            'o = console.output_view.get_buffer()\n' +
            'o.set_text("")\n' +
            'c = window.galtack_client\n' +
            'ld = reload_loadable(c)\n' +
            'o("GaltackClient instance: %r\\n" % c)\n' +
            'o("Loadable return value:  %r\\n" % ld)\n' +
            'c.send_commands((1, "message", "hi folks"))\n'
            )


class RawConsoleWithIntro(
        RawConsoleCenterInitMixin,
        RawConsoleWithIntroMixin,
        RawConsole,
        ): pass


class WindowWithRawConsole(Window):
    """
    Window with a RawConsole in it and starting at a reasonable size.
    """
    
    def __init__(self, *a, **kw):
        super(WindowWithRawConsole, self).__init__(*a, **kw)
        self.set_title("PyGTK Shell RawConsole")
        self.set_default_size(550, 400)
        self.raw_console = self(RawConsoleWithIntro())


class WindowF5RawConsoleMixin(Window):
    """
    Window opening a Window containing a RawConsole when F5 is pressed.
    """
    
    def __init__(self, *a, **kw):
        super(WindowF5RawConsoleMixin, self).__init__()
        self.connect("key-press-event", self.__cb_key_press_event)
    
    def __cb_key_press_event(self, window, event):
        if (KeyPressEval("F5"))(event):
            rc = WindowWithRawConsole().raw_console
            rc.code_view.textview_userexec_namespace["window"] = self
            return True
        return False


class WindowF5RawConsole(
        WindowF5RawConsoleMixin,
        Window,
        ): pass


### CUSTOMIZED STUFF FROM PYGTK SHELL [END]


def print_errback(failure):
    failure.printTraceback()
    return None


def reload_loadable(*a, **kw):
    m = sys.modules.get("galtack_loadable", None)
    if m:
        reload(m)
    else:
        import galtack_loadable as m
    return m.run_loadable(*a, **kw)


class GaltackChatWindow(WindowF5RawConsoleMixin):
    """
    A Window to allow chatting in Galcon, following the Galcon client
    protocol.
    """
    
    def __init__(self, prev_window, options, user_info, server_info):
        self.__class__._instance = self
        self.__prev_window  = prev_window
        self.__options      = options
        self.__user_info    = user_info
        self.__server_info  = server_info
        
        super(GaltackChatWindow, self).__init__()
        self.set_default_size(550, 300)
        n, o = server_info["name"], server_info["owner"]
        if n: n += " - "
        self.set_title("Chat (%s%s) - GalTacK" % (n, o))
        self.connect("delete-event", self.__cb_delete_event)
        
        outer_vbox, inner_vbox = gnome_hig(self)
        
        sw = inner_vbox(Frame())(ScrolledWindow())
        self.output_view = sw(TextView())
        self.__buf = self.output_view.get_buffer()
        self.__buf("Hint: Press <F5> to execute arbitrary Python code.\n")
        self.__buf.create_mark("end", self.__buf.get_end_iter(), False)
        self.__buf.connect("insert-text", self.__cb_insert_text)
        self.output_view.set_editable(False)
        hbox = gnome_hig(inner_vbox(HBox(), False, False))
        self.input_entry = hbox(Entry())
        self.send_button = hbox(Button("_Send"), False, False)
        self.send_button.connect("clicked", self.__cb_send_clicked)
        self.leave_button = hbox(Button("_Leave"), False, False)
        self.leave_button.connect("clicked", self.__cb_leave_clicked)
        self.quit_button = hbox(Button("_Quit"), False, False)
        self.quit_button.connect("clicked", self.__cb_quit_clicked)
        
        self.send_button.set_property("can-default", True)
        gobject.idle_add(self.send_button.grab_default)
        gobject.idle_add(self.input_entry.grab_focus)
        
        C = GaltackClient
        self.galtack_client = C(self.__options,
                                self.__user_info, self.__server_info)
        r = self.galtack_client.register_command_callback
        r("close", self.__cmd_close)
        r("message", self.__cmd_message)
        r("start", self.__cmd_start)
        r("stop", self.__cmd_stop)
        reactor.listenUDP(0, self.galtack_client)
        
        def f():  # TODO: implement this properly after login
            self.galtack_client.send_commands((1, "status", "away"))
            return False
        gobject.timeout_add(500, f)
    
    def __cb_delete_event(self, widget, event):
        self.leave_button.clicked()
        return True
    
    def __cb_insert_text(self, textbuffer, iter, text, length):
        mark = textbuffer.get_mark("end")
        if not mark:
            return False
        self.output_view.scroll_to_mark(mark, 0.0)
        return False
    
    def __cb_leave_clicked(self, button):
        self.set_sensitive(False)
        deferred = self.galtack_client.logout()
        deferred.addCallback(self.__cb_left)
        deferred.addErrback(self.__eb_left)
    
    def __cb_left(self, ignored_arg):
        # GaltackServerListWindow._instance.show()
        self.__prev_window.show()
        self.destroy()
    
    def __eb_left(self, ignored_arg):
        self.set_sensitive(True)
    
    def __cb_quit_clicked(self, button):
        self.set_sensitive(False)
        deferred = self.galtack_client.logout()
        deferred.addCallback(self.__cb_left_quit)
        deferred.addErrback(self.__cb_left_quit)
    
    def __cb_left_quit(self, ignored_arg):
        self.destroy()
    
    def __cb_send_clicked(self, button):
        msg = self.input_entry.get_text()
        self.galtack_client.send_commands((1, "message", msg))
        self.input_entry.set_text("")
    
    def __cmd_message(self, command):
        sender, message = command[3:]
        snd = ""
        if sender: snd = " " + sender
        self.__buf("%s%s %s\n" % (time.strftime("%X"), snd, message))

    def __cmd_close(self, command):
        self.galtack_client.send_commands((1, "[CLOSE]"))
        gobject.timeout_add(500, main_loop_quit)  # TODO: await ACK
        
    def __cmd_stop(self, command):
        self.__buf("(stop)\n")
        
    def __cmd_start(self, command):
        self.__buf("(start)\n")


class GaltackServerListWindow(Window):
    """
    Window displaying the list of Galcon servers.
    """

    NEXT_CLASS = GaltackChatWindow

    def __init__(self, prev_window, options, user_info):
        self.__class__._instance = self
        self.__prev_window = prev_window
        self.__options = options
        super(GaltackServerListWindow, self).__init__()
        self.__user_info = user_info
        self.set_title("Galcon Server List - GalTacK")
        self.set_default_size(600, 400)
        self.set_position(gtk.WIN_POS_CENTER)
        
        outer_vbox, self.__inner_vbox = gnome_hig(self)
        
        hbox = gnome_hig(self.__inner_vbox(HBox(), False, False))
        self.__version_label = hbox(LeftLabel(""), False, False)
        self.set_current_version()
        self.__version_label.set_sensitive(False)
        
        self.__refresh_button = hbox(Button("_Refresh List"), False, False,
                                     pack_end = True)
        self.__refresh_button.connect("clicked", self.__cb_refresh)
        
        sw = self.__inner_vbox(Frame())(ScrolledWindow())
        self.__treeview = sw(TreeView())
        self.__treeview.set_sensitive(False)
        self.__treeview.set_property("rules-hint", True)
        self.__treeview.connect("row-activated", self.__cb_row_activated)
        cb = self.__cb_selection_changed
        self.__treeview.get_selection().connect("changed", cb)
        
        for i, spec in enumerate(galtack.net.ServerInfo.COL_SPEC):
            if not spec["visible"]: continue
            col = gtk.TreeViewColumn(spec["caption"])
            col.set_reorderable(True)
            col.set_sort_column_id(i)
            self.__treeview.append_column(col)
            cell = gtk.CellRendererText()
            col.pack_start(cell, True)
            col.add_attribute(cell, "text", i)
            col.add_attribute(cell, "sensitive", 0)
        
        hbox = gnome_hig(self.__inner_vbox(HBox(), False, False))
        self.__join_button = hbox(Button("_Join"))
        self.__join_button.set_sensitive(False)
        self.__join_button.connect("clicked", self.__cb_join)
        
        self.back_button = hbox(Button("_Back"), False, False)
        self.back_button.connect("clicked", self.__cb_back)
        
        self.__statusbar = outer_vbox(Statusbar(), False, False)
        self.__statusbar_cid = cid = self.__statusbar.get_context_id("msg")
        
        self.__server_password_prompt = dlg = Dialog(
            "Server Password Required", self, gtk.DIALOG_MODAL,
            (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
             gtk.STOCK_OK, gtk.RESPONSE_ACCEPT)
        )
        dlg.set_default_response(gtk.RESPONSE_ACCEPT)
        dlg.set_default_size(300, -1)
        vbox = gnome_hig(VBox())  # I want my own VBox here ;-)
        vbox.set_border_width(6)
        dlg.vbox.pack_start(vbox)
        self.__server_password_prompt_label = vbox(LeftLabel())
        self.__server_password_prompt_entry = vbox(Entry())
        self.__server_password_prompt_entry.set_visibility(False)
        
    def set_current_version(self, current_version = None):
        self.__version_label.set_sensitive(False)
        if current_version is None:
            self.__version_label.set_text("Current version: ???")
        else:
            self.__version_label.set_text("Current version: %s" %
                                          ".".join(current_version))
            self.__version_label.set_sensitive(True)
    
    def set_server_info_list(self, server_info_list = None):
        types = galtack.net.ServerInfo.get_col_types()
        model = gtk.ListStore(*types)
        self.__treeview.set_sensitive(False)
        if server_info_list is not None:
            for server_info in server_info_list:
                model.append(server_info.get_data_tuple())
            self.__treeview.set_sensitive(True)
        self.__treeview.set_model(model)
    
    def __cb_refresh(self, button):
        self.window.set_cursor(watch_cursor)
        self.__inner_vbox.set_sensitive(False)
        self.__statusbar.push(self.__statusbar_cid,
                              "Retrieving server list...")
        deferred = galtack.net.get_server_list(self.__user_info)
        deferred.addCallback(self.__server_list_callback)
        deferred.addErrback(self.__server_list_errback)
        
    def __server_list_callback(self, (current_version, server_info_list)):
        self.set_current_version(current_version)
        self.set_server_info_list(server_info_list)
        self.__statusbar.pop(self.__statusbar_cid)
        self.__inner_vbox.set_sensitive(True)
        self.window.set_cursor(arrow_cursor)
    
    def __server_list_errback(self, failure):
        self.set_sensitive(True)
        self.window.set_cursor(arrow_cursor)
        cid = self.__statusbar_cid
        sbar = self.__statusbar
        sbar.pop(cid)
        gtk.gdk.beep()
        mid = sbar.push(cid, "Retrieving server list failed")
        gobject.timeout_add(4000, lambda: sbar.remove(cid, mid))
        return failure
    
    def __cb_selection_changed(self, treesel):
        model, iter = treesel.get_selected()
        if iter is None:
            ok_to_join = False
        else:
            data = galtack.net.ServerInfo(*model[iter])
            ok_to_join = data.bots_ok
        self.__join_button.set_sensitive(ok_to_join)
    
    def __cb_row_activated(self, treeview, path, view_column):
        self.__cb_join(self.__join_button)
    
    def __cb_join(self, button):
        treesel = self.__treeview.get_selection()
        model, iter = treesel.get_selected()
        server_info = galtack.net.ServerInfo(*model[iter])
        if server_info.pwd_protected:
            self.__run_server_password_prompt(server_info)
            return None
        self.join_with_server_info(server_info)
    
    def __cb_back(self, button):
        # self.GaltackLoginWindow._instance.show()
        self.__prev_window.show()
        self.destroy()
    
    def __run_server_password_prompt(self, server_info):
        n, o = server_info["name"], server_info["owner"]
        if n: n += " - "
        s = "Password for %s%s:" % (n, o)
        self.__server_password_prompt_label.set_text(s)
        response_id = self.__server_password_prompt.run()
        if response_id != gtk.RESPONSE_ACCEPT: return None
        passwd = self.__server_password_prompt_entry.get_text()
        server_info["passwd"] = passwd
        self.join_with_server_info(server_info)
    
    def join_with_server_info(self, server_info):
        chat_window = self.NEXT_CLASS(self, self.__options,
                                      self.__user_info, server_info)
        gobject.idle_add(self.hide)


class GaltackLoginWindow(Window):
    """
    A Window asking the user for Galcon login details and log in.
    """
    
    NEXT_CLASS = GaltackServerListWindow

    def __init__(self, options):
        self.__class__._instance = self
        self.__options = options
        super(GaltackLoginWindow, self).__init__()
        self.set_title("GalTacK Login")
        self.set_default_size(400, -1)
        self.set_position(gtk.WIN_POS_CENTER)
        
        outer_vbox, self.inner_vbox = gnome_hig(self)
        
        table = gnome_hig(self.inner_vbox(Table(), False, False))
        
        xop = {"xoptions": gtk.FILL}
        table.add_rows()
        label = table.attach_cell(LeftLabel("_Email Address:"), **xop)
        self.email_entry = table.attach_cell(Entry())
        label.set_mnemonic_widget(self.email_entry)
        if self.__options.email is not None:
            self.email_entry.set_text(self.__options.email)
        
        table.add_rows()
        label = table.attach_cell(LeftLabel("_Username:"), **xop)
        self.name_entry = table.attach_cell(Entry())
        label.set_mnemonic_widget(self.name_entry)
        if self.__options.user is not None:
            self.name_entry.set_text(self.__options.user)
        
        table.add_rows()
        label = table.attach_cell(LeftLabel("_Password:"), **xop)
        self.passwd_entry = table.attach_cell(Entry())
        label.set_mnemonic_widget(self.passwd_entry)
        self.passwd_entry.set_visibility(False)
        if self.__options.password is not None:
            self.passwd_entry.set_text(self.__options.password)
        
        table.add_rows()
        hbox = gnome_hig(table.attach_row(HBox()))
        self.__login_button = hbox(Button("_Sign In"))
        self.__login_button.connect("clicked", self.__cb_login)
        self.__login_button.set_property("can-default", True)
        gobject.idle_add(self.__login_button.grab_default)
        
        self.__quit_button = hbox(Button("_Quit"), False, False)
        self.__quit_button.connect("clicked", self.__cb_quit)
        
        self.statusbar = outer_vbox(Statusbar(), False, False)
        self.statusbar_cid = cid = self.statusbar.get_context_id("msg")
        mid = self.statusbar.push(cid, "Enter your login details")
        gobject.timeout_add(4000, lambda: self.statusbar.remove(cid, mid))
    
    def __get_user_info(self):
        email       = self.email_entry.get_text()
        name        = self.name_entry.get_text()
        passwd      = self.passwd_entry.get_text()
        platform    = "linux2"
        version     = "1.2.1"
        return galtack.net.UserInfo(email, name, passwd, platform, version)
    
    def __cb_login(self, button):
        self.window.set_cursor(watch_cursor)
        self.inner_vbox.set_sensitive(False)
        self.statusbar.push(self.statusbar_cid,
                            "Retrieving server list...")
        user_info = self.__get_user_info()
        deferred = galtack.net.get_server_list(user_info)
        deferred.addCallbacks(self.__server_list_callback,
                              self.__server_list_errback)
        deferred.addErrback(print_errback)
    
    def __cb_quit(self, button):
        main_loop_quit()
        
    def __server_list_callback(self, (current_version, server_info_list)):
        w = self.NEXT_CLASS(self, self.__options, self.__get_user_info())
        w.set_current_version(current_version)
        w.set_server_info_list(server_info_list)
        gobject.idle_add(self.hide)
        self.inner_vbox.set_sensitive(True)
        self.window.set_cursor(arrow_cursor)
        cid = self.statusbar_cid
        self.statusbar.pop(cid)
    
    def __server_list_errback(self, failure):
        failure.trap(galtack.net.ServerListException)
        self.inner_vbox.set_sensitive(True)
        self.window.set_cursor(arrow_cursor)
        cid = self.statusbar_cid
        self.statusbar.pop(cid)
        gtk.gdk.beep()
        mid = self.statusbar.push(cid, "Failure: %s" %
                                       failure.getErrorMessage())
        gobject.timeout_add(4000, lambda: self.statusbar.remove(cid, mid))
        return None
    
    @classmethod  # for potential inheritance
    def _modify_option_parser(cls, option_parser):
        option_parser.add_option("-e", "--email", metavar = "ADDRESS",
                                 help = "login email address")
        option_parser.add_option("-u", "--user",
                                 help = "login username")
        option_parser.add_option("-p", "--password",
                                 help = "login password")
        return option_parser


def main(argv):
    option_parser = optparse.OptionParser(prog = "GalTacK",
                                          version = "%%prog %s" %
                                          galtack_version_str)
    option_parser = GaltackLoginWindow._modify_option_parser(option_parser)
    option_parser = GaltackClient._modify_option_parser(option_parser)
    options, arguments = option_parser.parse_args(argv[1:])
    GaltackLoginWindow(options)
    main_loop_run()
    return 0

if __name__ == "__main__":
    import sys
    sys.exit(main(sys.argv))

